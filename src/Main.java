import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Main {
    public static int depth = 1;

    public static void main(String... args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter printWriter = new PrintWriter(System.out);

        String[] values = bufferedReader.readLine().split(" ");
        int N = Integer.parseInt(values[0]);

        ComputerFactory.init(N);

        int[] Ip = new int[4];
        int[] Mask = new int[4];

        for (int i = 0; i < N; i++) {
            int subNets = Integer.parseInt(bufferedReader.readLine().split(" ")[0]);
            for (int j = 0; j < subNets; j++) {
                values = bufferedReader.readLine().split(" ");
                String[] currentIp = values[0].split("[.]");
                String[] currentMask = values[1].split("[.]");

                String subnet = "";
                for (int k = 0; k < 4; k++) {
                    Ip[k] = Integer.parseInt(currentIp[k]);
                    Mask[k] = Integer.parseInt(currentMask[k]);
                    subnet += Integer.toBinaryString(Ip[k] & Mask[k]);
                }
                ComputerFactory.computers[i].subnets.add(subnet);
            }
        }

        for (Computer c : ComputerFactory.computers) {
            for (Computer d : ComputerFactory.computers) {
                if (c.index != d.index) {
                    boolean add = false;
                    for (String subnet : c.subnets)
                        if (d.subnets.contains(subnet))
                            add = true;
                    if (add)
                        c.neighbors.add(d);
                }
            }
        }

        values = bufferedReader.readLine().split(" ");
        int source = Integer.parseInt(values[0]) - 1;
        int destination = Integer.parseInt(values[1]) - 1;

        dfs(ComputerFactory.computers[destination]);

        Computer sourceComp = ComputerFactory.computers[source];
        if (sourceComp.parent == null)
            printWriter.println("No");
        else {
            printWriter.println("Yes");
            printWriter.print(sourceComp.index + 1);
            while (true) {
                sourceComp = sourceComp.parent;
                if (sourceComp == null)
                    break;
                printWriter.print(" " + (sourceComp.index + 1));
            }
        }
        printWriter.flush();
        printWriter.close();
    }

    private static void dfs(Computer parent) {
        parent.depth = depth++;
        for (Computer c : parent.neighbors) {
            if (c.depth == 0) {
                c.parent = parent;
                dfs(c);
            }
        }
        depth--;
    }
}

class Computer {
    public int index;
    public ArrayList<String> subnets = new ArrayList<>();
    public ArrayList<Computer> neighbors = new ArrayList<>();
    public Computer parent;
    public int depth;

    Computer(int index) {
        this.index = index;
    }
}

class ComputerFactory {
    public static Computer[] computers;

    public static void init(int N) {
        computers = new Computer[N];
        for (int i = 0; i < N; i++)
            computers[i] = new Computer(i);
    }
}
